#!/bin/sh

source sykle.conf

curl -sS -X POST -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: SSWS $APIKEY" -H "Cache-Control: no-cache" $1 | jq '.'

read -e -p "Enter passcode: " PASSCODE
echo $PASSCODE

curl -sS -X POST -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: SSWS $APIKEY" -H "Cache-Control: no-cache" -d "{ \"passCode\": \"$PASSCODE\" }" $1 | jq '.'