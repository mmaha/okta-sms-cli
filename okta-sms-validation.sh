#!/bin/sh

source sykle.conf

curl -sS -X GET -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: SSWS $APIKEY" -H "Cache-Control: no-cache" "https://$OKTAURL/api/v1/users/$USERID/factors" | jq ' .[] ' | jq ' select(.factorType == "sms")  ' | jq ' ._links.verify.href '